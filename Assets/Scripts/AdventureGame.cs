﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class AdventureGame : MonoBehaviour {
    [SerializeField] State startingState;
    [SerializeField] State endingState;
    [SerializeField] State firstCaseState;
    [SerializeField] State hiddenEndingState;
    [SerializeField] State[] endings;

    [SerializeField] GameObject endingsLabel;
    [SerializeField] GameObject endingsNumberLabel;

    [SerializeField] TextMeshProUGUI textComponent;

    [SerializeField] string mainMenu;
    [SerializeField] int amountOfEndings;

    State state;
    bool hasFinishedGame = false;
    Dictionary<string, bool> endingsDictionary = new Dictionary<string, bool>();
    int endingsCounter = 0;

    // Start is called before the first frame update
    void Start() {
        state = startingState;

        UpdateBodyText();

        HideEndingsTexts();

        CreateStatesDictionary();
    }

    private void CreateStatesDictionary() {
         foreach (State state in endings) {
            endingsDictionary.Add(state.name, false);
        }
        /*
        foreach (KeyValuePair<string, bool> kvp in endingsDictionary) {
            print(kvp);
        }
        */
    }

    private void UpdateBodyText() {
        textComponent.text = state.GetStateStory();
    }

    private void HideEndingsTexts() {
        endingsLabel.SetActive(false);
        endingsNumberLabel.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        ManageState();

        if (Input.GetKey(KeyCode.Q)) {
            SceneManager.LoadScene(mainMenu);
        }
    }

    private void ManageState() {
        if (hasFinishedGame && state == firstCaseState) {
            ShowEndingsTexts();
        }

        string stateName = state.name; // Variable inhereted from the ScriptableObject class

        CheckIfStateIsEnding(stateName);
        
        State[] nextStates = state.GetNextStates();
        
        ChangeState(nextStates);

        UpdateBodyText();
    }

    private void CheckIfStateIsEnding(string stateName) {
        if (endingsDictionary.ContainsKey(stateName)){
            endingsDictionary[stateName] = true;
            hasFinishedGame = true;
            UpdateEndingsCounter();
        } else {
        }
    }

    private void ChangeState(State[] nextStates) {
        for (int i = 0; i < nextStates.Length; i++) {

            if (Input.GetKeyDown(KeyCode.Alpha1 + i)) {
                state = nextStates[i];
            }
        }

        if (endingsCounter == endingsDictionary.Count && state == endingState) {
            state = hiddenEndingState;
        }

    }

    private void UpdateEndingsCounter() {
        CountEndingsSeen();

        endingsNumberLabel.GetComponent<TextMeshProUGUI>().text = endingsCounter.ToString() + "/" + amountOfEndings.ToString();
    }

    private void CountEndingsSeen() {
        int counter = 0;
        foreach (KeyValuePair<string, bool> keyValuePair in endingsDictionary) {
            if (keyValuePair.Value == true) {
                counter++;
            }
        }
        endingsCounter = counter;
    }
    
    private void ShowEndingsTexts() {
        endingsLabel.SetActive(true);
        endingsNumberLabel.SetActive(true);
    }
}
