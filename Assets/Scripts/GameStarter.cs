﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameStarter : MonoBehaviour
{
    [SerializeField] string englishScene;
    [SerializeField] string spanishScene;

    public void StartGameInEnglish() {
        SceneManager.LoadScene(englishScene);
    }

    public void StartGameInSpanish() {
        SceneManager.LoadScene(spanishScene);
    }
}
